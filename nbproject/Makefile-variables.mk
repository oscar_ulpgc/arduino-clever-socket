#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=Arduino-MacOSX
CND_ARTIFACT_DIR_Debug=dist/Debug/Arduino-MacOSX
CND_ARTIFACT_NAME_Debug=arduino-clever-socket
CND_ARTIFACT_PATH_Debug=dist/Debug/Arduino-MacOSX/arduino-clever-socket
CND_PACKAGE_DIR_Debug=dist/Debug/Arduino-MacOSX/package
CND_PACKAGE_NAME_Debug=arduino-clever-socket.tar
CND_PACKAGE_PATH_Debug=dist/Debug/Arduino-MacOSX/package/arduino-clever-socket.tar
# Release configuration
CND_PLATFORM_Release=GNU-MacOSX
CND_ARTIFACT_DIR_Release=dist/Release/GNU-MacOSX
CND_ARTIFACT_NAME_Release=arduino-clever-socket
CND_ARTIFACT_PATH_Release=dist/Release/GNU-MacOSX/arduino-clever-socket
CND_PACKAGE_DIR_Release=dist/Release/GNU-MacOSX/package
CND_PACKAGE_NAME_Release=arduino-clever-socket.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-MacOSX/package/arduino-clever-socket.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
