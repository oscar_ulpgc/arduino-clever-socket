# Arduino Clever Socket
This is an Arduino-based Web Server library.

## Features

- URL parameter parsing
- Handel the following HTTP methods: GET, POST
- JSON/RESTful interface
- Web Authentication

## How it works

### About authentication

All communications with Arduino Clever Socket API must be authenticated with username and password credentials.

The credentials have to be concatenated with a colon like user:user.

This example uses the following credentials:

- Username: user
- Password: user

### Get pin values

HTML request: http://arduino.cleversocket.es

JSON request: http://arduino.cleversocket.es/?format=json

### Change pin values

POST to URL: http://arduino.cleversocket.es

E.g.: Set digital pin 2 to HIGH and digital pin 5 to LOW:
d2=1&d5=0