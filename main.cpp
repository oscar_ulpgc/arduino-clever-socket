#include <SPI.h>
#include <Ethernet.h>
#include <Streaming.h>
#include <WebServer.h>

static uint8_t mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
static uint8_t ip[] = {192, 168, 1, 210};

#define PREFIX ""

WebServer webserver(PREFIX, 80);

void outputPins(WebServer &server, WebServer::ConnectionType type) {
    server.httpSuccess("application/json; charset=utf-8");
    
    int i;
    server << "{";
    for (i = 0; i <= 9; ++i) {
        // ignore the pins we use to talk to the Ethernet chip
        int val = digitalRead(i);
        server << "\"d" << i << "\":" << val << ",";
    }

    for (i = 0; i <= 5; ++i) {
        int val = analogRead(i);
        server << "\"a" << i << "\":" << val;
        if (i != 5)
            server << ",";
    }

    server << "}";
}

#define NAMELEN 32
#define VALUELEN 32
#define FORMATLEN 8

void defaultCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete) {
    /* credentials to get access
     * username = user
     * password = user
     * "dXNlcjp1c2Vy" is the Base64 representation of "user:user"
     */
    if (server.checkCredentials("dXNlcjp1c2Vy")) {
        if (type == WebServer::POST) {
            bool repeat;
            char name[NAMELEN];
            char value[VALUELEN];
            do {
                repeat = server.readPOSTparam(name, NAMELEN, value, VALUELEN);
                if (name[0] == 'a') {
                    int pin = strtoul(name + 1, NULL, 10);
                    int val = strtoul(value, NULL, 10);
                    analogWrite(pin, val);
                } else if (name[0] == 'd') {
                    int pin = strtoul(name + 1, NULL, 10);
                    int val = strtoul(value, NULL, 10);
                    digitalWrite(pin, val);
                }
            } while (repeat);
        }

        outputPins(server, type);
    } else {
        server.httpUnauthorized();
    }
}

void setup() {
    // set pins 0-8 for digital input
    for (int i = 0; i <= 9; ++i)
        pinMode(i, OUTPUT);
    pinMode(9, OUTPUT);

    Ethernet.begin(mac, ip);
    webserver.begin();

    webserver.setDefaultCommand(&defaultCmd);
}

void loop() {
    webserver.processConnection();
}